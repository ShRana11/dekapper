//
//  HairStyleItems.swift
//  DeKapper
//
//  Created by MacStudent on 2018-08-11.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import Foundation
class HairStyleItems{
    static var style : [String] = ["Low Fade with Long Fringe", "High Fade with Fohawk", "High Fade with Loose Pompadour", "High Fade with Hard Part ", "Messy Hair with Low Fade", "Braided Razor Part with Pompadour", "High Bald Fade with Cropped Fringe", "Short Sides with Medium Length Hair", "Hard Part Pompadour with Mid Bald Fade", "Low Fade with Thick Long Hair", "Mid Fade with Side Part ", "Tall Thick Spiky Hair on Top"]
    static var stylePrice : [String] = ["$20", "$30", "$40", "$30", "$50", "$20" , "$40", "$30", "$50", "$30", "$20", "$40"]
    static var description : [String] = ["Low Fade with Long Fringe", "High Fade with Fohawk", "High Fade with Loose Pompadour", "High Fade with Hard Part ", "Messy Hair with Low Fade", "Braided Razor Part with Pompadour", "High Bald Fade with Cropped Fringe", "Short Sides with Medium Length Hair", "Hard Part Pompadour with Mid Bald Fade", "Low Fade with Thick Long Hair", "Mid Fade with Side Part ", "Tall Thick Spiky Hair on Top"]
}
