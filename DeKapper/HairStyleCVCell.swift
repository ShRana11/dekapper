//
//  HairStyleCVCell.swift
//  DeKapper
//
//  Created by MacStudent on 2018-08-11.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit

class HairStyleCVCell: UICollectionViewCell {
    
    @IBOutlet weak var lblHairStyle: UILabel!
    @IBOutlet weak var imgHairStyle: UIImageView!
}
