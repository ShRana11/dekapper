//
//  HairStyleDataVC.swift
//  DeKapper
//
//  Created by MacStudent on 2018-08-14.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit

let kKey_UD_TIMING_SLOT = "timing"
let kKey_CV_TIME_CELL   = "TimeCell"

class HairStyleDataVC: UIViewController ,UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var lblCollectionView: UILabel!
    @IBOutlet weak var imgCollectionView: UIImageView!
    @IBOutlet weak var timeSelectCVCell: UICollectionViewCell!
    @IBOutlet weak var timeSelectCVC: UICollectionView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgHairStyle: UIImageView!
    @IBOutlet weak var lbldatePick: UIDatePicker!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblShowDescription: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblShowPrice: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    
    let defaults = UserDefaults.standard
    var time: [String] = ["10:00 AM", "11:00 AM", "12:00 AM", "1:00 PM", "2:00 PM", "3:00 PM", "4:00 PM", "5:00 PM", "6:00 PM", "7:00 PM", "8:00 PM"]
    var minDate = Date()
    var maxDate = Date()
    var a : Int?
    var index : String?
    var list:[String :[[String : Bool]]] = [:]
    var plistArray = NSMutableArray()
    var selectedHairStyle : Int?
    var formattedDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDateTime()
        if defaults.object(forKey: kKey_UD_TIMING_SLOT) != nil {
            list = defaults.object(forKey: kKey_UD_TIMING_SLOT) as! [String : [[String : Bool]]]
        }
        formattedDate = updateDateFormat(date: lbldatePick.date)
        lbldatePick.minimumDate = minDate
        lbldatePick.maximumDate = maxDate
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedHairStyle != nil{
            self.fetchDetails()
            self.displayDetails()
            setDateTime()
            lbldatePick.minimumDate = minDate
            lbldatePick.maximumDate = maxDate
        }
        timeSelectCVC.reloadData()
    }
    
    func fetchDetails(){
        if let filePath = Bundle.main.path(forResource: "Style", ofType: "plist") {
            //get an array representation of plist
            plistArray = NSMutableArray(contentsOfFile: filePath)!
            print("plistArray \(plistArray)")
        }
    }
    
    func setDateTime(){
        let currentDate = Date()
        var dateComponents = DateComponents()
        let calendar = Calendar.init(identifier: .gregorian)
        dateComponents.day = 0
        minDate = calendar.date(byAdding: dateComponents, to: currentDate)!
        dateComponents.day = +10
        maxDate = calendar.date(byAdding: dateComponents, to: currentDate)!
        lbldatePick.minimumDate = minDate
        lbldatePick.maximumDate = maxDate
    }
    
    @IBAction func btnConfirm(_ sender: Any) {
        let infoAlert = UIAlertController(title: "Want to Book An Appointment", message: "Date: \(lbldatePick.date), Timing: \(index!)", preferredStyle: .alert)
          showAlert(alertval: infoAlert)
    }
    func showAlert(alertval : Any) {
        
        self.present(alertval as! UIViewController, animated: true, completion: nil)
        let action1 = UIAlertAction.init(title: "Confirm", style: .default) { (_) in
            print("CONGRATS... Appointment has been Booked")
            let mainSB : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let resultTVC = mainSB.instantiateViewController(withIdentifier: "HairStyleScene");            self.navigationController?.pushViewController(resultTVC, animated: true)
            
            
        }
        (alertval as AnyObject).addAction(action1)
        let action2 = UIAlertAction.init(title: "Cancel", style: .cancel) { (_) in
            let mainSB : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let resultTVC = mainSB.instantiateViewController(withIdentifier: "HairStyleScene")
                        self.navigationController?.pushViewController(resultTVC, animated: true)
            //self.present(resultTVC, animated: true, completion: nil)
        }
        (alertval as AnyObject).addAction(action2)
        
    }
    func displayDetails(){
        lblShowPrice.text = HairStyleItems.stylePrice[self.selectedHairStyle!]
        let details = plistArray[self.selectedHairStyle!] as! NSMutableDictionary
        self.lblShowDescription.text = details.value(forKey: "Describe") as? String
        self.imgHairStyle.image = UIImage(named: details.value(forKey: "Image") as! String)
        self.lblShowPrice.text = details.value(forKey: "Price") as? String
        print("\(details.value(forKey: "Price") as! String)")
       //self.lbldatePick.date = (details.value(forKey: "Date") as? Date)!
    }
    
    //MARK: Collection view handling begins
    //MARK: Collection View datasource methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return time.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: kKey_CV_TIME_CELL, for: indexPath)
        //Do rest of the code
        var state: Bool = false
        let formattedDate: String = updateDateFormat(date: lbldatePick.date)
        let imgView: UIImageView = cell.viewWithTag(1) as! UIImageView
        let slotLabel: UILabel = cell.viewWithTag(2) as! UILabel
        if list[formattedDate] != nil{
            for arr in list[formattedDate]! {
                let dict: [String:Bool] = arr
                if dict[time[indexPath.row]] != nil{
                    state = dict[time[indexPath.row]]!
                }
            }
        }
        imgView.image = #imageLiteral(resourceName: "ok")
        if state {
            imgView.image = #imageLiteral(resourceName: "cancel")
        }
        slotLabel.text = time[indexPath.row]
        index = slotLabel.text
        return cell
    }
    
    //MARK: Collection view delegate methods
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Code to perform the selection of the collection view cell
        print(lbldatePick.date)
        print(time[indexPath.row])
        if list[formattedDate] != nil{
            list[formattedDate]?.append([time[indexPath.row]: true])
        } else {
            let arr = [[time[indexPath.row]: true]]
            list[formattedDate] = arr
        }
        defaults.set(list, forKey: kKey_UD_TIMING_SLOT)
        timeSelectCVC.reloadItems(at: [indexPath])
        print(list)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        //Do coding to disable/enable selection of a particular cell at specific indexPath
        if list[formattedDate] != nil{
            for arr in list[formattedDate]! {
                let dict: [String:Bool] = arr
                if dict[time[indexPath.row]] != nil{
                    return false
                }
            }
        }
        return true
    }
    //MARK: Collection view handling ends
    
    //MARK: Handler for Date update
    @IBAction func updateDate(_ sender: UIDatePicker) {
        print("Date updated")
        formattedDate = updateDateFormat(date: sender.date)
        timeSelectCVC.reloadData()
    }
    
    //MARK: Change the format of the Date
    func updateDateFormat(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter.string(from:date)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
