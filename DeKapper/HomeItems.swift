//
//  HomeItems.swift
//  DeKapper
//
//  Created by MacStudent on 2018-08-11.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import Foundation
class HomeItems{
    static var titles : [String] = ["Saloon", "Chat", "Location", "Logout"]
    static var subTitles : [String] = ["Hair-Cut Section", "Talk with Professionals", "Find us on Map", "Be in Touch! Bye"]
    static var images : [String] = ["Saloon", "Chat", "Navigation", "Logout"]
}
