//
//  ContactDetailsVC.swift
//  DeKapper
//
//  Created by MacStudent on 2018-08-17.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit
import CallKit
import MessageUI
class ContactdetailVC : UIViewController {
    
    var contactNumber: String = ""
    var contactName : String = ""
    var contactEmail : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Contacts", style: .plain, target: self, action: #selector(self.goToContacts))
        self.navigationItem.title = contactName
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCallAction(_ sender: Any) {
            print("calling...")
            let url = URL(string: "tel://+11231231231")
            
            if UIApplication.shared.canOpenURL(url!){
                if #available(iOS 10, *)
                {
                    UIApplication.shared.open(url!)
                }
                else
                {
                    UIApplication.shared.openURL(url!)
                }
            }
        
    }
   
    @IBAction func btnChatAction(_ sender: Any) {
        print("Messaging...")
        if MFMessageComposeViewController.canSendText() {
            
            let messageVC = MFMessageComposeViewController()
            
            messageVC.body = "Hello, How are you?"
            messageVC.recipients = ["+11231231231"]
            messageVC.messageComposeDelegate = self as? MFMessageComposeViewControllerDelegate
            
            self.present(messageVC,animated: true,completion: nil)
        }
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        //for Message
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            self.dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            self.dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            self.dismiss(animated: true, completion: nil)
        }
        
        //for Email
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
   
    @IBAction func btnEmailAction(_ sender: Any) {
        print("sending email...")
        
        if MFMailComposeViewController.canSendMail() {
            
            let EmailPicker = MFMailComposeViewController()
            
            EmailPicker.mailComposeDelegate = self as! MFMailComposeViewControllerDelegate
            EmailPicker.setSubject("Test Email")
            EmailPicker.setMessageBody("Hello, How are you!", isHTML: true)
            
            self.present(EmailPicker ,animated: true, completion: nil)
            
        }
        else{
            print("email sent...")
        }
    }
    
    @objc func goToContacts(){
        self.navigationController?.popViewController(animated: true)
    }
    
}
/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */


