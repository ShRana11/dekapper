//
//  HomeTVC.swift
//  DeKapper
//
//  Created by MacStudent on 2018-08-11.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit

class HomeTVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 110


        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return HomeItems.titles.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeTVCell

        // Configure the cell...
        cell.lblTitle.text = HomeItems.titles[indexPath.row]
        cell.lblSubtitle.text = HomeItems.subTitles[indexPath.row]
        cell.imgHome.image = UIImage(named: HomeItems.images[indexPath.row])

        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        switch indexPath.row {
        case 0:
            print("Saloon Selecteded")
            let mainSB : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let hairStyleCVC = mainSB.instantiateViewController(withIdentifier: "HairStyleScene") as! HairStyleCVC
            //self.present(hairStyleCVC, animated: true, completion: nil)
            navigationController?.pushViewController(hairStyleCVC, animated: true)
        case 1:
            let mainSB : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let zodiacCVC = mainSB.instantiateViewController(withIdentifier: "ContactdetailScene") as! ContactdetailVC
            navigationController?.pushViewController(zodiacCVC, animated: true)
        case 2:
            let mainSB : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let maper = mainSB.instantiateViewController(withIdentifier: "MapScene") as! Map
            navigationController?.pushViewController(maper, animated: true)
            
            
        case 3:
            let mainSB : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let maper = mainSB.instantiateViewController(withIdentifier: "LoginScene") as! LoginVC
            navigationController?.pushViewController(maper, animated: true)
            

        default:
            print("No Action")
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
