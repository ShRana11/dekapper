//
//  ViewController.swift
//  DeKapper
//
//  Created by Sukhwinder Rana on 2019-06-27.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSignUp.layer.shadowColor = UIColor.white.cgColor
        btnSignUp.layer.shadowOpacity = 0.7
        btnSignUp.layer.shadowOffset = CGSize(width: 7, height: 5)
        btnSignIn.layer.shadowColor = UIColor.white.cgColor
        btnSignIn.layer.shadowOpacity = 0.7
        btnSignIn.layer.shadowOffset = CGSize(width: 7, height: 5)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func btnSignUpSelect(_ sender: Any) {
        let mainSB : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let registerVC = mainSB.instantiateViewController(withIdentifier: "RegisterScene")
        // self.present(registerVC, animated: true, completion: nil)
        navigationController?.pushViewController(registerVC, animated: true)
        
    }
    @IBAction func btnSignInSelect(_ sender: UIButton) {
        
        let mainSB : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = mainSB.instantiateViewController(withIdentifier: "LoginScene")
        // self.present(loginVC, animated: true, completion: nil)
        navigationController?.pushViewController(loginVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


