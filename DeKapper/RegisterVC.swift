//
//  RegisterVC.swift
//  DeKapper
//
//  Created by MacStudent on 2018-08-11.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var registerSwitchGender: UISegmentedControl!
    @IBOutlet weak var registerDatePicker: UIDatePicker!
    @IBOutlet weak var txtRegisterPostalCode: UITextField!
    @IBOutlet weak var registerPickerView: UIPickerView!
    @IBOutlet weak var txtRegisterAddress: UITextField!
    @IBOutlet weak var txtRegisterContactNo: UITextField!
    @IBOutlet weak var txtRegisterConfirmPassword: UITextField!
    @IBOutlet weak var txtRegisterPassword: UITextField!
    @IBOutlet weak var txtRegisterEmail: UITextField!
    @IBOutlet weak var txtRegisterName: UITextField!
    static var mail: String?
    static var pass: String?
    let cityList : [String] = ["Toronto", "Vancouver", "Banff", "Jasper", "Ajax", "Alberta", "Ottawa", "Kitchener", "Brampton", "Mississauga"]
    var gender: String!
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Sign Up"
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        let btnSubmit = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(displayInfo))
        self.navigationItem.rightBarButtonItem = btnSubmit
    }
    @objc func displayInfo(){
        var userInfo : String = txtRegisterName.text!
        userInfo += "\n" + txtRegisterContactNo.text!
        userInfo += "\n" + txtRegisterAddress.text!
        userInfo += "\n" + txtRegisterEmail.text!
        RegisterVC.mail = txtRegisterEmail.text!
        userInfo += "\n" + txtRegisterPassword.text!
        RegisterVC.pass = txtRegisterPassword.text!
        userInfo += "\n \(registerDatePicker.date)"
        
        userInfo += "\n \(cityList[registerPickerView.selectedRow(inComponent: 0)])"
        
        switch registerSwitchGender.selectedSegmentIndex {
        case 0:
            userInfo += "\n Male"
            gender = "Male"
        case 1:
            userInfo += "\n Female"
            gender = "Female"
        case 2:
            userInfo += "\n Others"
            gender = "Others"
        default:
            userInfo += "\n Others"
            gender = "Others"
        }
        
        let infoAlert = UIAlertController(title: "Verify Details", message: userInfo, preferredStyle: .alert)
        
        if txtRegisterPassword.text == txtRegisterConfirmPassword.text{
            infoAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: {_ in self.displayHomeVC()}))
        }else{
            infoAlert.message = "Both passwords must be same"
        }
        
        infoAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(infoAlert, animated: true, completion: nil)
        
    }
    
    func displayHomeVC(){
        var newUser = User(name: txtRegisterName.text!, address: txtRegisterAddress.text!, contactNumber: txtRegisterContactNo.text!, postalCode: txtRegisterPostalCode.text!, city: cityList[registerPickerView.selectedRow(inComponent: 0)], email: txtRegisterEmail.text!, password: txtRegisterPassword.text!, gender: self.gender, dob: registerDatePicker.date)
        
        if User.addUser(newUser: newUser) {
            let mainSB : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeVC = mainSB.instantiateViewController(withIdentifier: "LoginScene")
            navigationController?.pushViewController(homeVC, animated: true)
        }else{
            let infoAlert = UIAlertController(title: "User Account", message: "An account with this email address already exist.", preferredStyle: .alert)
            infoAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(infoAlert,animated: true)
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cityList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cityList[row]
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerPickerView.delegate = self as? UIPickerViewDelegate
        registerPickerView.dataSource = self as? UIPickerViewDataSource
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
